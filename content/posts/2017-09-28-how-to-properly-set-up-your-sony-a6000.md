+++
title = "How to properly set up your Sony A6000 camera (Part I)"
date = "2017-09-28"
thumbnail = "/img/a6000.jpg"
categories = ["Photography"]
tags = ["photography", "Sony A6000"]
+++

During my summer holidays, I decided to buy my first digital camera. After a thorough market research, I finally chose one of Sony's best sellers: the **Mirorless A6000**.

Having no idea about all the settings I had to deal with, I started googling for days, wathed various youtube videos, read blog posts, articles etc. Below you will find some of the best videos I found on youtube that helped me customize my A6000 and begin trying to learn how to shoot in manual mode:


### General Settings
&nbsp;
{{< youtube iGq9qXhYCBU >}}
&nbsp;
{{< youtube O4oXwxVZLDM >}}
&nbsp;
{{< youtube 6_SlKQbWYa8 >}}
&nbsp;
{{< youtube RYXwCGWb7Yg >}}
&nbsp;
&nbsp;
### Autofocus Settings
&nbsp;
{{< youtube a479RxVDWto >}}
&nbsp;
{{< youtube TjaVyAzwRyg >}}
&nbsp;
{{< youtube L88uEmUp4t8 >}}
&nbsp;

The videos I highly recommend ar the ones by [that1cameraguy](https://www.youtube.com/user/that1cameraguy). This guy is amazing: he has created three videos of more than 80 minutes in total, trying to explain each one of the settings found in A6000 menu.

If you want to learn almost everything about the sony's autofocus settings then you should wath the included videos by [Gary Fong](https://www.youtube.com/user/GFIGARYFONG). Although his suggestions are not my daily driver, it's always good to know what this camera offers and know how to use continuous autofocus with moving objecs or people, face recognition etc. when needed.

However If you don't have the time to watch them all, just spend as much time as you can on the first three videos and after that will you have a very good idea on what this brilliant camera can do.
