+++
title = "About"
+++

I am a passionate Linux user since 2003. 

Long time ago I created the [Archphile Project](https://archphile.org).

I spend most of my free time listening to music, shooting with my Fuji, torturing various ARM boards and flashing any device that accepts a custom firmware.


**About this website:**
The Penguin was created using [hugo](https://gohugo.io/ ) framework and [hugo-xmin](https://themes.gohugo.io/hugo-xmin/) theme.
